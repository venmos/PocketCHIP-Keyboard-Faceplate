# PocketCHIP Keyboard Faceplate
files for 3D-printable keyboard faceplate for PocketCHIP version 1.0. This design is based off of <a href="https://github.com/NextThingCo/PocketCHIP-Mechanical" target="_blank">NTC's STEP CAD files</a>.

# Description
Tired thumbs from lots of typing on your PocketCHIP? Games got your hands sore? This faceplate is designed to provide a more familiar - and less fatiguing - typing and gaming experience.

# Print Recommendations for Desktop FFF/FDM 3D Printers
0.2mm layer height with 36% infill has sufficed for me. No supports necessary. I have had success with both PLA and PETG. Be very careful when removing the keys from the print surface - the connections between the keys are thin, as they are meant to be flexible. PETG is nice for this purpose.

# Installation
After inserting the keys through the back of the faceplate, it is ready to be installed. This faceplate replaces the bezel that comes with PocketCHIP, so remove the bezel. Then, taking care so that the keys do not fall out of the faceplate, snap into place. I find it is easiest to start with the two clips at the very top and work down. Some satisfying clicks later, you are good to go! A video of the installation can be found <a href="https://youtu.be/OewZ7DTLPB0" target="_blank">here</a>.

# Removal
Removal is simple. Like the installation, I find it easiest to start with the clips at the very top. Carefully pop the clips off, and there you go!

# License
<a rel="license" href="http://creativecommons.org/licenses/by-sa/3.0/us/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/3.0/us/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/3.0/us/">Creative Commons Attribution-ShareAlike 3.0 United States License</a>.

